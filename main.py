from flask import Flask, render_template,request, redirect,url_for
import psycopg2

app= Flask(__name__)

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/empleados')
def empleados():
    return render_template('empleados.html')

@app.route('/clases')
def clases():
    return render_template('clases.html')

@app.route('/horario')
def horario():
    return render_template('horario.html')

if __name__ == '__main__':
    app.run(debug=True)


